import { ProductType } from "./api";

export type Option = {
  value: any;
  title: string;
};

export enum SortingOptions {
  PRICE_ASC = "price|asc",
  PRICE_DESC = "price|desc",
  DATE_ASC = "added|asc",
  DATE_DESC = "added|desc",
}

export type BasketItemType = {
  product: ProductType;
  count: number;
};
