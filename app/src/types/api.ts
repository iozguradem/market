export type CompanyType = {
  account: number;
  name: string;
  slug: string;
};

export type ProductType = {
  tags: string[];
  price: number;
  name: string;
  slug: string;
};

export enum ProductCategoryTypes {
  SHIRT = "shirt",
  MUG = "mug",
}
