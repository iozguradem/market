import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface IFilterState {
  totalCount: number | null;
  currentPage: number;
}

const initialState: IFilterState = {
  totalCount: null,
  currentPage: 1,
};

export const paginationSlice = createSlice({
  name: "pagination",
  initialState,
  reducers: {
    setCurrentPage: (state, action: PayloadAction<number>) => {
      state.currentPage = action.payload;
    },
    setTotalCount: (state, action: PayloadAction<number>) => {
      state.totalCount = action.payload;
    },
  },
});

export const { setCurrentPage, setTotalCount } = paginationSlice.actions;

export default paginationSlice.reducer;
