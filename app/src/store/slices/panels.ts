import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface IFilterState {
  filterPanel: boolean;
  basketPanel: boolean;
}

const initialState: IFilterState = {
  filterPanel: false,
  basketPanel: false,
};

export const panelsSlice = createSlice({
  name: "panels",
  initialState,
  reducers: {
    setFilterPanel: (state, action: PayloadAction<boolean>) => {
      state.filterPanel = action.payload;
      if (state.filterPanel) {
        state.basketPanel = false;
      }
    },
    setBasketPanel: (state, action: PayloadAction<boolean>) => {
      state.basketPanel = action.payload;
      if (state.basketPanel) {
        state.filterPanel = false;
      }
    },
  },
});

export const { setFilterPanel, setBasketPanel } = panelsSlice.actions;

export default panelsSlice.reducer;
