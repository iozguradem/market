import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ProductCategoryTypes } from "../../types/api";
import { SortingOptions } from "../../types/shared";

interface IFilterState {
  sorting: SortingOptions;
  brands: string[];
  tags: string[];
  category: ProductCategoryTypes;
}

const initialState: IFilterState = {
  sorting: SortingOptions.PRICE_ASC,
  brands: [],
  tags: [],
  category: ProductCategoryTypes.MUG,
};

export const filterSlice = createSlice({
  name: "filter",
  initialState,
  reducers: {
    setSorting: (state, action: PayloadAction<SortingOptions>) => {
      state.sorting = action.payload;
    },
    pushBrand: (state, action: PayloadAction<string>) => {
      state.brands.push(action.payload);
    },
    removeBrand: (state, action: PayloadAction<string>) => {
      state.brands = state.brands.filter(
        (item: string) => item !== action.payload
      );
    },
    pushMultipleBrand: (state, action: PayloadAction<string[]>) => {
      state.brands.push(...action.payload);
    },
    removeMultipleBrand: (state, action: PayloadAction<string[]>) => {
      state.brands = state.brands.filter(
        (item: string) => !action.payload.includes(item)
      );
    },
    pushTag: (state, action: PayloadAction<string>) => {
      state.tags.push(action.payload);
    },
    removeTag: (state, action: PayloadAction<string>) => {
      state.tags = state.tags.filter((item: string) => item !== action.payload);
    },
    pushMultipleTag: (state, action: PayloadAction<string[]>) => {
      state.tags.push(...action.payload);
    },
    removeMultipleTag: (state, action: PayloadAction<string[]>) => {
      state.tags = state.tags.filter(
        (item: string) => !action.payload.includes(item)
      );
    },
    setCategory: (state, action: PayloadAction<ProductCategoryTypes>) => {
      state.category = action.payload;
    },
  },
});

export const {
  setSorting,
  pushBrand,
  removeBrand,
  pushMultipleBrand,
  removeMultipleBrand,
  pushTag,
  removeTag,
  pushMultipleTag,
  removeMultipleTag,
  setCategory,
} = filterSlice.actions;

export default filterSlice.reducer;
