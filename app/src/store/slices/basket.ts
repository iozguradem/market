import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ProductType } from "../../types/api";
import { BasketItemType } from "../../types/shared";
interface IFilterState {
  items: BasketItemType[];
}

const initialState: IFilterState = {
  items: [],
};

export const basketSlice = createSlice({
  name: "basket",
  initialState,
  reducers: {
    addProductToBasket: (state, action: PayloadAction<ProductType>) => {
      let basketItem = state.items.find(
        (item) => item.product.slug === action.payload.slug
      );

      if (basketItem === undefined) {
        state.items.push({
          product: action.payload,
          count: 1,
        });
      } else {
        state.items = state.items.map((item) => {
          if (item.product.slug === action.payload.slug) {
            item.count++;
          }
          return item;
        });
      }
    },
    incrementCountBySlug: (state, action: PayloadAction<string>) => {
      state.items = state.items.map((item) => {
        if (item.product.slug === action.payload) {
          item.count++;
        }
        return item;
      });
    },

    decrementCountBySlug: (state, action: PayloadAction<string>) => {
      const basketItem = state.items.find(
        (item) => item.product.slug === action.payload
      );

      if (basketItem && basketItem.count <= 1) {
        state.items = state.items.filter(
          (item) => item.product.slug !== action.payload
        );
        return;
      }

      state.items = state.items.map((item) => {
        if (item.product.slug === action.payload) {
          item.count--;
        }
        return item;
      });
    },
  },
});

export const {
  addProductToBasket,
  incrementCountBySlug,
  decrementCountBySlug,
} = basketSlice.actions;

export default basketSlice.reducer;
