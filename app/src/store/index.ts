import { configureStore } from "@reduxjs/toolkit";
import basket from "./slices/basket";
import filters from "./slices/filters";
import pagination from "./slices/pagination";
import panels from "./slices/panels";

export const store = configureStore({
  reducer: {
    basket,
    filters,
    pagination,
    panels,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
