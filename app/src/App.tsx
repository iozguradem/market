import { Provider } from "react-redux";
import axios from "axios";
import { store } from "./store";
import Content from "./Content";

axios.defaults.baseURL = "https://api.ozguradem.net";
if (window.location.hostname === "hostname") {
  axios.defaults.baseURL = "http://localhost:4000";
}

function App() {
  return (
    <Provider store={store}>
      <Content />
    </Provider>
  );
}

export default App;
