import { Option } from "../types/shared";

export const sortAsc = (items: any[], key: string) => {
  return items.sort((a: any, b: any) => {
    if (a[key] < b[key]) {
      return -1;
    }
    if (a[key] > b[key]) {
      return 1;
    }
    return 0;
  });
};

export const sortStringArrayAsc = (items: string[]) => {
  return items.sort((a: any, b: any) => {
    if (a < b) {
      return -1;
    }
    if (a > b) {
      return 1;
    }
    return 0;
  });
};

export const filterOptionsBySeachText = (options: Option[], text: string) => {
  return options.filter(
    (option: Option) =>
      !text ||
      text.length === 0 ||
      option.title.toLocaleLowerCase().search(text) > -1
  );
};

export const ALL_OPTION: Option = {
  value: "ALL",
  title: "All",
};

export const toCurrency = (price: number): string => {
  return `₺ ${price.toFixed(2)}`;
};

export const getVisiblePages = (
  currentPage: number,
  pageCount: number
): number[] => {
  let visiblePages: number[] = [currentPage];

  for (let index: number = 1; index < 3; index++) {
    visiblePages.unshift(currentPage - index);
    visiblePages.push(currentPage + index);
  }

  return visiblePages.filter((page) => page >= 1 && page <= pageCount);
};

export const getTotalPageCount = (totalCount: number | null): number => {
  let pageCount: number = 0;

  if (totalCount) {
    pageCount = Math.ceil(totalCount / 16);
  }

  return pageCount;
};
