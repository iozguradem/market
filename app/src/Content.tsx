import styled, { createGlobalStyle } from "styled-components";
import Header from "./components/Header";
import PageContent from "./components/PageContent";
import { useAppSelector } from "./store/hooks";

type StyleProps = {
  isMenuOpen: boolean;
};

const GlobalStyle = createGlobalStyle<StyleProps>`
  body {
    @media only screen and (max-width: 1334px) {
      overflow: ${(props) => (props.isMenuOpen ? "hidden" : "scroll")};
    }
  }
`;

const Container = styled.div``;

export default function Content() {
  const panels = useAppSelector((state) => state.panels);

  return (
    <>
      <GlobalStyle isMenuOpen={panels.filterPanel || panels.basketPanel} />
      <Container>
        <Header />
        <PageContent />
      </Container>
    </>
  );
}
