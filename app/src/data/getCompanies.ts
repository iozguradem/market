import axios from "axios";
import { Option } from "../types/shared";
import { CompanyType } from "../types/api";
import { sortAsc } from "../helpers";

const getCompanies = async (): Promise<Option[]> => {
  const { data } = await axios.get("companies");
  const items = data.map((i: CompanyType): Option => {
    const option: Option = {
      value: i.slug,
      title: i.name,
    };
    return option;
  });
  sortAsc(items, "title");
  return items;
};

export default getCompanies;
