import axios from "axios";
import { ProductType, ProductCategoryTypes } from "../types/api";

type ParamType = {
  _sort: string;
  _order: string;
  _page: number;
  _limit: number;
  itemType: string;
  tags_like: string[] | undefined;
  manufacturer: string[] | undefined;
};

type PaginationResponse = {
  items: ProductType[];
  totalCount: number;
};

const paginateItems = async (
  sort: string,
  order: string,
  page: number,
  tags: string[],
  manufacturer: string[],
  category: ProductCategoryTypes
): Promise<PaginationResponse> => {
  const params: ParamType = {
    _sort: sort,
    _order: order,
    _page: page,
    _limit: 16,
    itemType: category,
    tags_like: undefined,
    manufacturer: undefined,
  };

  if (tags.length) {
    params.tags_like = tags;
  }

  if (manufacturer.length) {
    params.manufacturer = manufacturer;
  }

  const response = await axios.get("items", { params });

  const result: PaginationResponse = {
    items: response.data,
    totalCount: parseInt(response.headers["x-total-count"]),
  };

  return result;
};

export default paginateItems;
