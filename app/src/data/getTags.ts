import axios from "axios";
import { ProductType } from "../types/api";
import { sortStringArrayAsc } from "../helpers";
import { Option } from "../types/shared";

const getTags = async (): Promise<Option[]> => {
  const { data } = await axios.get("items");
  const tags: string[] = [];

  data.forEach((item: ProductType) => {
    item.tags.forEach((tag: string) => {
      if (!tags.includes(tag)) {
        tags.push(tag);
      }
    });
  });

  sortStringArrayAsc(tags);

  return tags.map((tag: string) => {
    return {
      value: tag,
      title: tag,
    };
  });
};

export default getTags;
