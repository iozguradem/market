import getCompanies from "./getCompanies";
import getTags from "./getTags";

export { getCompanies, getTags };
