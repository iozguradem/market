import styled from "styled-components";
import FilterSection from "../Sections/FilterSection";
import BasketSection from "../Sections/BasketSection";
import Products from "../Products";

const Container = styled.div`
  max-width: 1234px;
  margin: auto;
  position: relative;
  padding: 115px 0px;
  display: flex;

  @media only screen and (max-width: 768px) {
    padding: 70px 0px;
  }
`;

export default function PageContent() {
  return (
    <Container>
      <FilterSection />
      <Products />
      <BasketSection />
    </Container>
  );
}
