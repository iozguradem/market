import styled from "styled-components";
const Container = styled.div`
  margin-bottom: 24px;
`;

const Title = styled.p`
  margin: 0px;
  font-weight: 600;
  font-size: 13px;
  line-height: 18px;
  color: #697488;
`;

const Box = styled.div`
  margin-top: 12px;
  background: #ffffff;
  box-shadow: 0px 6px 24px rgba(93, 62, 188, 0.04);
  border-radius: 2px;
  padding: 24px;
`;

type FilterBoxProps = {
  title: string;
  children: JSX.Element;
};

export default function FilterBox({ title, children }: FilterBoxProps) {
  return (
    <Container>
      <Title>{title}</Title>
      <Box>{children}</Box>
    </Container>
  );
}
