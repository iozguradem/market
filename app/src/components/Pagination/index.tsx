import styled from "styled-components";
import Page from "./Page";
import { ArrowLeft, ArrowRight } from "../Icons";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { setCurrentPage } from "../../store/slices/pagination";
import { getVisiblePages, getTotalPageCount } from "../../helpers";

const Container = styled.div`
  display: flex;
  justify-content: center;
`;

const PrevPage = styled(Page)`
  margin-right: 6px;

  @media only screen and (max-width: 768px) {
    display: none;
  }
`;

const NextPage = styled(Page)`
  margin-left: 6px;

  @media only screen and (max-width: 768px) {
    display: none;
  }
`;

export default function Pagination() {
  const pagination = useAppSelector((state) => state.pagination);
  const dispatch = useAppDispatch();

  const handleClick = (page: number) => {
    dispatch(setCurrentPage(page));
  };

  const { currentPage, totalCount } = pagination;
  const pageCount = getTotalPageCount(totalCount);
  const visiblePages = getVisiblePages(currentPage, pageCount);

  return (
    <Container>
      <PrevPage
        onClick={() => handleClick(currentPage - 1)}
        disabled={currentPage === 1}
      >
        <ArrowLeft />
        Prev
      </PrevPage>
      {!visiblePages.includes(1) && (
        <>
          <Page onClick={() => handleClick(1)}>1</Page>
          <Page disabled>...</Page>
        </>
      )}
      {visiblePages.map((page) => (
        <Page
          key={page}
          active={page === currentPage}
          onClick={() => handleClick(page)}
        >
          {page}
        </Page>
      ))}
      {!visiblePages.includes(pageCount) && (
        <>
          <Page disabled>...</Page>
          <Page onClick={() => handleClick(pageCount)}>{pageCount}</Page>
        </>
      )}
      <NextPage
        onClick={() => handleClick(currentPage + 1)}
        disabled={currentPage === pageCount}
      >
        Next
        <ArrowRight />
      </NextPage>
    </Container>
  );
}
