import styled from "styled-components";
import BaseButton from "./BaseButton";

const Container = styled(BaseButton)`
  background: #1ea4ce;
  border-radius: 2px;
  font-weight: 600;
  font-size: 12px;
  line-height: 20px;
  color: #ffffff;
  width: 100%;
  padding: 1px 0px;
  cursor: pointer;
`;

interface IButtonProps extends React.HTMLAttributes<HTMLButtonElement> {
  children: JSX.Element | string;
}

export default function Button({ children, ...props }: IButtonProps) {
  return <Container {...props}>{children}</Container>;
}
