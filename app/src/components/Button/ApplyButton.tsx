import styled from "styled-components";
import Button from ".";

const Container = styled(Button)`
  display: none;

  @media only screen and (max-width: 1334px) {
    &.is-open-on-mobile {
      display: unset;
      padding: 10px;
    }
  }
`;

type ApplyButtonProps = {
  isActive: boolean;
  onClick: () => void;
};

export default function ApplyButton({ isActive, onClick }: ApplyButtonProps) {
  return (
    <Container
      className={isActive ? "is-open-on-mobile" : ""}
      onClick={onClick}
    >
      Apply
    </Container>
  );
}
