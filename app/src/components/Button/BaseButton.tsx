import styled from "styled-components";

const Container = styled.button`
  border: none;

  &:focus {
    outline: none;
  }
`;

interface IButtonProps extends React.HTMLAttributes<HTMLButtonElement> {
  type?: string;
  children: JSX.Element | string;
}

export default function BaseButton({
  type = "button",
  children,
  ...props
}: IButtonProps) {
  return (
    <Container type="button" {...props}>
      {children}
    </Container>
  );
}
