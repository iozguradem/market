import styled from "styled-components";
import logo from "./logo.svg";

const LogoImage = styled.img`
  margin-left: auto;
  margin-top: 17px;
  margin-bottom: 19px;

  @media only screen and (max-width: 768px) {
    height: 20px;
  }
`;

export default function Logo() {
  return <LogoImage src={logo} alt="Logo" />;
}
