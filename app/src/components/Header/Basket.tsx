import styled from "styled-components";
import basket from "./basket.svg";
import TotalBasketPrice from "../Shared/TotalBasketPrice";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { setBasketPanel } from "../../store/slices/panels";

const Container = styled.button`
  position: absolute;
  right: 0px;
  top: 0px;
  background-color: #147594;
  margin-left: auto;
  display: flex;
  align-items: center;
  padding: 0px 24px;
  height: 100%;
  border: none;

  &:focus {
    outline: none;
  }

  @media only screen and (max-width: 768px) {
    padding: 0px 6px;
  }
`;

const BasketImage = styled.img`
  margin-right: 2px;
`;

const TotalPrice = styled.span`
  font-weight: 600;
  font-size: 14px;
  line-height: 18px;
  letter-spacing: 0.16px;
  color: white;
`;

export default function Basket() {
  const panels = useAppSelector((state) => state.panels);
  const dispatch = useAppDispatch();

  const handleClick = () => {
    dispatch(setBasketPanel(!panels.basketPanel));
  };

  return (
    <Container type="button" onClick={handleClick}>
      <BasketImage src={basket} />
      <TotalPrice>
        <TotalBasketPrice />
      </TotalPrice>
    </Container>
  );
}
