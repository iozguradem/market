import styled from "styled-components";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { setFilterPanel } from "../../store/slices/panels";
import { FilterIcon } from "../Icons";

const Container = styled.button`
  position: absolute;
  display: none;
  background: transparent;
  border: none;
  cursor: pointer;
  padding: 5px;
  color: white;

  &:focus {
    outline: none;
  }

  &:hover {
    color: #f1f1f1;
  }

  @media only screen and (max-width: 1334px) {
    left: 16px;
    top: 24px;
    display: unset;
  }

  @media only screen and (max-width: 768px) {
    left: 14px;
    top: 14px;
    display: unset;
  }
`;

export default function FilterButton() {
  const panels = useAppSelector((state) => state.panels);
  const dispatch = useAppDispatch();

  const handleClick = () => {
    dispatch(setFilterPanel(!panels.filterPanel));
  };

  return (
    <Container onClick={handleClick}>
      <FilterIcon />
    </Container>
  );
}
