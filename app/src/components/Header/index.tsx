import styled from "styled-components";
import Logo from "./Logo";
import Basket from "./Basket";
import FilterButton from "./FilterButton";

const Container = styled.div`
  background-color: #1ea4ce;
  color: #ffffff;
  text-align: center;

  position: fixed;
  width: 100%;
  z-index: 999;
`;

const BoundaryBox = styled.div`
  max-width: 1234px;
  margin: auto;
  position: relative;
`;

export default function Header() {
  return (
    <Container>
      <BoundaryBox>
        <FilterButton />
        <Logo />
        <Basket />
      </BoundaryBox>
    </Container>
  );
}
