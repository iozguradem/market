import styled from "styled-components";
import { ProductType } from "../../types/api";
import Button from "../Button";
import { toCurrency } from "../../helpers";
import { useAppDispatch } from "../../store/hooks";
import { addProductToBasket } from "../../store/slices/basket";

const Container = styled.div`
  margin-bottom: 20px;
  margin-right: 24px;
  max-width: 124px;

  &:nth-child(4n) {
    margin-right: 0px;
  }

  @media only screen and (max-width: 1334px) {
    &:nth-child(4n) {
      margin-right: 24px;
    }

    max-width: unset;
    display: flex;
    width: calc(50% - 24px);
  }

  @media only screen and (max-width: 992px) {
    &:nth-child(4n) {
      margin-right: 24px;
    }

    max-width: unset;
    display: flex;
    width: calc(100% - 24px);
  }

  @media only screen and (max-width: 576px) {
    &:nth-child(4n) {
      margin-right: 0px;
    }

    margin: 5px;
  }
`;

const ProductImageContainer = styled.div`
  background: #fefefe;
  border: 1px solid #f3f0fe;
  border-radius: 12px;
  padding: 16px;

  @media only screen and (max-width: 576px) {
    padding: 4px;
  }
`;

const ProductImageCenterer = styled.div``;

type ProductImageProps = {
  src: string;
};

const ProductImage = styled.div<ProductImageProps>`
  width: 92px;
  height: 92px;
  line-height: 92px;
  background-image: url(${(props) => props.src});
  background-size: cover;
  background-repeat: no-repeat;

  @media only screen and (max-width: 576px) {
    border-radius: 12px;
  }
`;

const DataSection = styled.div`
  height: 94px;
  position: relative;
  @media only screen and (max-width: 1334px) {
    padding: 0px 0px 20px 20px;
    margin-right: 20px;
    width: 100%;
    position: relative;
  }

  @media only screen and (max-width: 576px) {
    padding: 0px 0px 5px 5px;
    margin-right: 0px;
  }
`;

const Price = styled.p`
  margin: 8px 0px 3px 0px;
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  color: #1ea4ce;
`;

const Title = styled.p`
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  color: #191919;
  margin: 0px 0px 8px 0px;

  @media only screen and (max-width: 576px) {
    font-size: 14px;
    line-height: 18px;
    font-weight: 500;
  }
`;

const StyledButton = styled(Button)`
  bottom: 0px;
  position: absolute;

  @media only screen and (max-width: 1334px) {
    position: absolute;
    bottom: 10px;
    padding: 5px 0px;
  }

  @media only screen and (max-width: 576px) {
    bottom: 0px;
  }
`;

type ProductCardProps = {
  product: ProductType;
};

export default function ProductCard({ product }: ProductCardProps) {
  const randomImageCount = Math.floor(Math.random() * 15) + 1;
  const dispatch = useAppDispatch();

  const handleAdd = () => {
    dispatch(addProductToBasket(product));
  };

  return (
    <Container>
      <ProductImageContainer>
        <ProductImageCenterer>
          <ProductImage src={`/product-images/${randomImageCount}.jpg`} />
        </ProductImageCenterer>
      </ProductImageContainer>
      <DataSection>
        <Price>{toCurrency(product.price)}</Price>
        <Title>{product.name}</Title>
        <StyledButton onClick={handleAdd}>Add</StyledButton>
      </DataSection>
    </Container>
  );
}
