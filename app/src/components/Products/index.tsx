import styled from "styled-components";
import ProductList from "./ProductList";
import Pagination from "../Pagination";
import ProductCategoryFilter from "./ProductCategoryFilter";

const Container = styled.div`
  width: 100%;
  padding: 0px 16px;
  border: 1px solid transparent;
`;

const Title = styled.div`
  font-family: Open Sans;
  font-weight: normal;
  font-size: 20px;
  line-height: 26px;
  letter-spacing: 0.25px;
  color: #6f6f6f;
  margin-bottom: 16px;
`;

export default function Products() {
  return (
    <Container>
      <Title>Products</Title>
      <ProductCategoryFilter />
      <ProductList />
      <Pagination />
    </Container>
  );
}
