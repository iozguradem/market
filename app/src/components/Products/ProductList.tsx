import { useEffect, useState } from "react";
import styled from "styled-components";
import paginateItems from "../../data/paginateItems";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { ProductType } from "../../types/api";
import ProductCard from "./ProductCard";
import { setTotalCount } from "../../store/slices/pagination";

const Container = styled.div`
  margin-top: 16px;
  background: #ffffff;
  box-shadow: 0px 4px 24px rgba(93, 62, 188, 0.04);
  border-radius: 2px;
  padding: 20px 20px 0px 20px;
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 32px;

  @media only screen and (max-width: 576px) {
    padding: 5px 5px 0px 5px;
  }
`;

export default function ProductList() {
  const filters = useAppSelector((state) => state.filters);
  const pagination = useAppSelector((state) => state.pagination);
  const dispatch = useAppDispatch();
  const [products, setProducts] = useState<ProductType[]>([]);

  useEffect(() => {
    const [sort, order] = filters.sorting.split("|");
    paginateItems(
      sort,
      order,
      pagination.currentPage,
      filters.tags,
      filters.brands,
      filters.category
    ).then((response) => {
      dispatch(setTotalCount(response.totalCount));
      setProducts(response.items);
    });
  }, [filters, pagination.currentPage, dispatch]);

  return (
    <Container>
      {products.map((product, index) => (
        <ProductCard key={index} product={product} />
      ))}
    </Container>
  );
}
