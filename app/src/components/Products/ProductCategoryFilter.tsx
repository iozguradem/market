import ToggleButton from "../Form/ToggleButton";
import { Option } from "../../types/shared";
import { ProductCategoryTypes } from "../../types/api";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { setCategory } from "../../store/slices/filters";

const PRODUCT_TYPES: Option[] = [
  { value: ProductCategoryTypes.MUG, title: "mug" },
  { value: ProductCategoryTypes.SHIRT, title: "shirt" },
];

export default function ProductCategoryFilter() {
  const filters = useAppSelector((state) => state.filters);
  const dispatch = useAppDispatch();

  const handleChange = (value: string) => {
    dispatch(setCategory(value as ProductCategoryTypes));
  };

  return (
    <ToggleButton
      options={PRODUCT_TYPES}
      current={filters.category}
      onChange={handleChange}
    />
  );
}
