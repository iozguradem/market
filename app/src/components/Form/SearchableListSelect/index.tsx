import styled from "styled-components";
import TextInput from "../TextInput";
import CheckBox from "../CheckBox";
import { Option } from "../../../types/shared";
import { useState } from "react";
import { filterOptionsBySeachText, ALL_OPTION } from "../../../helpers";

const ListContainer = styled.div`
  padding-top: 17px;
`;

const ScrollBox = styled.div`
  overflow-y: scroll;
  height: 130px;
  padding: 10px;
  margin-left: -9px;

  &::-webkit-scrollbar-track {
    background-color: white;
  }

  &::-webkit-scrollbar {
    width: 4px;
    border-radius: 4px;
  }

  &::-webkit-scrollbar-thumb {
    background-color: #e0e0e0;
    border-radius: 4px;
  }
`;

type SearchableListSelectProps = {
  placeholder: string;
  options: Option[];
  values: any[];
  onAllChange: (
    event: React.ChangeEvent<HTMLInputElement>,
    options: any[]
  ) => void;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
};

export default function SearchableListSelect({
  placeholder,
  options,
  values,
  onAllChange,
  onChange,
}: SearchableListSelectProps) {
  const [searchText, setSearchText] = useState("");

  const filteredOptions = filterOptionsBySeachText(options, searchText);

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value.toLocaleLowerCase());
  };

  const handleAllOptionChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const selections: number[] = filteredOptions.map((i: Option) => i.value);
    onAllChange(event, selections);
  };

  return (
    <>
      <TextInput placeholder={placeholder} onChange={handleSearchChange} />
      <ListContainer>
        <ScrollBox>
          <CheckBox option={ALL_OPTION} onChange={handleAllOptionChange} />
          {filteredOptions.map((option, index) => (
            <CheckBox
              key={index}
              option={option}
              onChange={onChange}
              isChecked={values.includes(option.value)}
            />
          ))}
        </ScrollBox>
      </ListContainer>
    </>
  );
}
