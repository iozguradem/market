import styled from "styled-components";

const Input = styled.input`
  width: 100%;
  border: 2px solid #e0e0e0;
  box-sizing: border-box;
  border-radius: 2px;
  outline: none;
  padding: 12px 16px;

  font-weight: normal;
  font-size: 14px;
  line-height: 24px;
  letter-spacing: 0.15px;
  color: #525252;

  ::placeholder {
    color: #a8a8a8;
  }
`;

interface ITextInputProps extends React.HTMLAttributes<HTMLInputElement> {}

export default function TextInput({ ...props }: ITextInputProps) {
  return <Input {...props} />;
}
