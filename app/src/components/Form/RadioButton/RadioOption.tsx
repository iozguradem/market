import styled from "styled-components";
import checkIcon from "./check.svg";
import { Option } from "../../../types/shared";

const Item = styled.label`
  display: block;
  position: relative;
  padding-left: 34px;
  margin-bottom: 16px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  font-weight: normal;
  font-size: 14px;
  line-height: 18px;
  letter-spacing: 0.16px;
  color: #525252;
  padding-top: 2px;

  :last-child {
    margin-bottom: 0px;
  }

  input:checked ~ .checkmark {
    background-color: white;
    border: 2px solid #1ea4ce;
  }

  input:checked ~ .checkmark:after {
    display: block;
  }

  .checkmark:after {
    background-image: url(${checkIcon});
    width: 8px;
    height: 7px;
    left: 5px;
    top: 5px;
  }
`;

const StyledRadioButton = styled.input`
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
`;

const CheckMark = styled.span`
  position: absolute;
  top: 0;
  left: 0;
  height: 22px;
  width: 22px;
  background: #ffffff;
  border: 2px solid #dfdee2;
  border-radius: 50%;

  :after {
    content: "";
    position: absolute;
    display: none;
  }
`;

type RadioOptionProps = {
  name: string;
  item: Option;
  value?: string;
  onChange: (value: string) => void;
};

export default function RadioOption({
  name,
  value,
  item,
  onChange,
}: RadioOptionProps) {
  return (
    <Item>
      {item.title}
      <StyledRadioButton
        type="radio"
        value={item.value}
        name={name}
        checked={item.value === value}
        onChange={() => onChange(item.value)}
      />
      <CheckMark className="checkmark" />
    </Item>
  );
}
