import RadioOption from "./RadioOption";
import { Option } from "../../../types/shared";

type RadioButtonProps = {
  name: string;
  value?: string;
  items: Option[];
  onChange: (value: string) => void;
};

export default function RadioButton({
  name,
  value,
  items,
  onChange,
}: RadioButtonProps) {
  return (
    <>
      {items.map((item, index) => (
        <RadioOption
          key={index}
          name={name}
          item={item}
          value={value}
          onChange={onChange}
        />
      ))}
    </>
  );
}
