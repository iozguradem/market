import styled from "styled-components";
import { Option } from "../../../types/shared";

type StyleProps = {
  active: boolean;
};

const StyledButton = styled.button<StyleProps>`
  margin-right: 8px;
  padding: 6px 16px;
  font-weight: 600;
  font-size: 13px;
  line-height: 18px;
  text-align: center;
  border-radius: 2px;
  outline: none;
  border: none;
  cursor: pointer;

  &:focus {
    outline: 0;
  }

  background: ${(props) => (props.active ? "#1ea4ce" : "#f2f0fd")};
  color: ${(props) => (props.active ? "#F2F0FD" : "#1ea4ce")};
`;

type ToggleButtonProps = {
  current: string;
  options: Option[];
  onChange: (value: string) => void;
};

export default function ToggleButton({
  current,
  options,
  onChange,
}: ToggleButtonProps) {
  return (
    <>
      {options.map((option, index) => (
        <StyledButton
          type="button"
          key={index}
          active={option.value === current}
          onClick={() => onChange(option.value)}
        >
          {option.title}
        </StyledButton>
      ))}
    </>
  );
}
