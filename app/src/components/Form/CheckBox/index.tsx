import styled from "styled-components";
import checkIcon from "./check.svg";
import { Option } from "../../../types/shared";

const Item = styled.label`
  display: block;
  position: relative;
  padding-left: 34px;
  margin-bottom: 16px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  font-weight: normal;
  font-size: 14px;
  line-height: 18px;
  letter-spacing: 0.16px;
  color: #525252;
  padding-top: 2px;

  :last-child {
    margin-bottom: 0px;
  }

  input:checked ~ .checkmark {
    background-color: #1ea4ce;
    border: 2px solid #1ea4ce;
  }

  input:checked ~ .checkmark:after {
    display: block;
  }

  .checkmark:after {
    background-image: url(${checkIcon});
    background-repeat: no-repeat;
    width: 12px;
    height: 12px;
    left: 3px;
    top: 5px;
  }
`;

const StyledRadioButton = styled.input`
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
`;

const CheckMark = styled.span`
  position: absolute;
  top: 0;
  left: 0;
  height: 22px;
  width: 22px;
  background: #ffffff;
  box-shadow: 0px 1px 7px rgba(93, 56, 192, 0.4);
  border-radius: 2px;

  :after {
    content: "";
    position: absolute;
    display: none;
  }
`;

type CheckBoxProps = {
  option: Option;
  isChecked?: boolean;
  onChange: (value: any) => void;
};

export default function CheckBox({
  option,
  isChecked,
  onChange,
}: CheckBoxProps) {
  return (
    <Item>
      {option.title}
      <StyledRadioButton
        type="checkbox"
        value={option.value}
        checked={isChecked}
        onChange={onChange}
      />
      <CheckMark className="checkmark" />
    </Item>
  );
}
