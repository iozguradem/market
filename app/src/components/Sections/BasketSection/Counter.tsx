import styled from "styled-components";
import { useAppDispatch } from "../../../store/hooks";
import { DecrementIcon, IncrementIcon } from "../../Icons";
import {
  incrementCountBySlug,
  decrementCountBySlug,
} from "../../../store/slices/basket";

const Container = styled.div`
  display: flex;
`;

const StyledButton = styled.button`
  background: transparent;
  cursor: pointer;
  border: none;

  &:focus {
    outline: none;
  }
`;

const Data = styled.div`
  background: #1ea4ce;
  font-family: Open Sans;
  font-weight: bold;
  font-size: 15px;
  line-height: 20px;
  text-align: center;
  color: #ffffff;
  padding: 5px 10px;
`;

type CounterProps = {
  slug: string;
  count: number;
};

export default function Counter({ slug, count }: CounterProps) {
  const dispatch = useAppDispatch();

  const handleDecrement = () => {
    dispatch(decrementCountBySlug(slug));
  };

  const handleIncrement = () => {
    dispatch(incrementCountBySlug(slug));
  };

  return (
    <Container>
      <StyledButton type="button" onClick={handleDecrement}>
        <DecrementIcon />
      </StyledButton>
      <Data>{count}</Data>
      <StyledButton type="button" onClick={handleIncrement}>
        <IncrementIcon />
      </StyledButton>
    </Container>
  );
}
