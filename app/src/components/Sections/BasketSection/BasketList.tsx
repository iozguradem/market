import styled from "styled-components";
import { useAppSelector } from "../../../store/hooks";
import BasketItem from "./BasketItem";

const EmptyMessage = styled.div`
  font-size: 14px;
  line-height: 18px;
  letter-spacing: 0.16px;
  background: #f9f9f9;
  padding: 10px;
  border-radius: 4px;
  margin-bottom: 20px;
  color: #555;
`;

export default function BastList() {
  const basket = useAppSelector((state) => state.basket);

  return (
    <>
      {basket.items.length === 0 && (
        <EmptyMessage>There is not any item on the basket yet.</EmptyMessage>
      )}
      {basket.items.map((item, index) => (
        <BasketItem key={index} item={item} />
      ))}
    </>
  );
}
