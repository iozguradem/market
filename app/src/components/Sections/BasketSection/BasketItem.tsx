import styled from "styled-components";
import { BasketItemType } from "../../../types/shared";
import Counter from "./Counter";
import { toCurrency } from "../../../helpers";

const Container = styled.div`
  padding: 10px 4px 18px 4px;
  border-bottom: 1px solid #f4f4f4;
  display: flex;
`;

const DataBox = styled.div`
  font-size: 14px;
  line-height: 18px;
  letter-spacing: 0.16px;
`;

const ProductTitle = styled.p`
  margin: 0px;
  font-weight: normal;
  color: #191919;
  padding-bottom: 4px;
`;

const BasketItemPrice = styled.p`
  margin: 0px;
  font-weight: 600;
  color: #1ea4ce;
`;

const AmountBox = styled.div`
  margin-left: auto;
  align-items: center;
`;

type BasketItemProps = {
  item: BasketItemType;
};

export default function BasketItem({ item }: BasketItemProps) {
  return (
    <Container>
      <DataBox>
        <ProductTitle>{item.product.name}</ProductTitle>
        <BasketItemPrice>
          {toCurrency(item.product.price * item.count)}
        </BasketItemPrice>
      </DataBox>
      <AmountBox>
        <Counter slug={item.product.slug} count={item.count} />
      </AmountBox>
    </Container>
  );
}
