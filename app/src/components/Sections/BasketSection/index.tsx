import styled from "styled-components";
import ApplyButton from "../../Button/ApplyButton";
import BastList from "./BasketList";
import TotalBasketPrice from "../../Shared/TotalBasketPrice";
import { useAppDispatch, useAppSelector } from "../../../store/hooks";
import { setBasketPanel } from "../../../store/slices/panels";

const Container = styled.div`
  min-width: 296px;
  margin-left: auto;
  background-color: #fafafa;

  @media only screen and (max-width: 992px) {
    display: none;
  }

  @media only screen and (max-width: 992px) {
    &.is-open-on-mobile {
      display: block;
      position: fixed;
      top: 56px;
      left: 0px;
      width: 100%;
      z-index: 1;
      padding: 20px 20px 100px 20px;
      height: 100%;
      overflow: scroll;
    }
  }
`;

const BlueBox = styled.div`
  background-color: #1ea4ce;
  padding: 8px;
  border-radius: 2px;
  margin-bottom: 20px;
`;

const Basket = styled.div`
  background: white;
  border-radius: 2px;
  padding: 16px;
`;

const PriceContainer = styled.div`
  display: flex;
  justify-content: right;
`;

const PriceBlueBox = styled.div`
  background: #1ea4ce;
  border-radius: 2px;
  margin-top: 10px;
  border: none;
  display: inline-block;
`;

const Price = styled.div`
  background: #ffffff;
  border: 2px solid #1ea4ce;
  box-sizing: border-box;
  border-radius: 2px;
  padding: 17px 24px;

  font-weight: 600;
  font-size: 14px;
  line-height: 16px;
  color: #1ea4ce;
`;

export default function BasketSection() {
  const panels = useAppSelector((state) => state.panels);
  const dispatch = useAppDispatch();

  const handleClick = () => {
    dispatch(setBasketPanel(!panels.basketPanel));
  };

  return (
    <Container className={panels.basketPanel ? "is-open-on-mobile" : ""}>
      <BlueBox>
        <Basket>
          <BastList />
          <PriceContainer>
            <PriceBlueBox>
              <Price>
                <TotalBasketPrice />
              </Price>
            </PriceBlueBox>
          </PriceContainer>
        </Basket>
      </BlueBox>
      <ApplyButton isActive={panels.basketPanel} onClick={handleClick} />
    </Container>
  );
}
