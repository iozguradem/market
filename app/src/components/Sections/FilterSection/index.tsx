import styled from "styled-components";
import SortingFilter from "./SortingFilter";
import BrandsFilter from "./BrandsFilter";
import TagsFilter from "./TagsFilter";
import ApplyButton from "../../Button/ApplyButton";
import { useAppDispatch, useAppSelector } from "../../../store/hooks";
import { setFilterPanel } from "../../../store/slices/panels";

const Container = styled.div`
  min-width: 296px;
  background-color: #fafafa;

  @media only screen and (max-width: 1334px) {
    display: none;
  }

  @media only screen and (max-width: 1334px) {
    &.is-open-on-mobile {
      display: block;
      position: fixed;
      top: 56px;
      left: 0px;
      width: 100%;
      z-index: 1;
      padding: 20px 20px 100px 20px;
      height: 100%;
      overflow: scroll;
    }
  }
`;

export default function FilterSection() {
  const panels = useAppSelector((state) => state.panels);
  const dispatch = useAppDispatch();

  const handleClick = () => {
    dispatch(setFilterPanel(!panels.filterPanel));
  };

  return (
    <Container className={panels.filterPanel ? "is-open-on-mobile" : ""}>
      <SortingFilter />
      <BrandsFilter />
      <TagsFilter />
      <ApplyButton isActive={panels.filterPanel} onClick={handleClick} />
    </Container>
  );
}
