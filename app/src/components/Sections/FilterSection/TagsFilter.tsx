import { useEffect, useState } from "react";
import FilterBox from "../../FilterBox";
import SearchableListSelect from "../../Form/SearchableListSelect";
import { Option } from "../../../types/shared";
import { getTags } from "../../../data";
import {
  pushTag,
  removeTag,
  pushMultipleTag,
  removeMultipleTag,
} from "../../../store/slices/filters";
import { useAppDispatch, useAppSelector } from "../../../store/hooks";

export default function TagsFilter() {
  const [tags, setTags] = useState<Option[]>([]);
  const filters = useAppSelector((state) => state.filters);
  const dispatch = useAppDispatch();

  useEffect(() => {
    getTags().then((tags: any[]) => setTags(tags));
  }, []);

  const handleItemChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      dispatch(pushTag(event.target.value));
    } else {
      dispatch(removeTag(event.target.value));
    }
  };

  const handleAllChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    options: string[]
  ) => {
    if (event.target.checked) {
      dispatch(pushMultipleTag(options));
    } else {
      dispatch(removeMultipleTag(options));
    }
  };

  return (
    <FilterBox title="Tags">
      <SearchableListSelect
        placeholder="Search Tags"
        options={tags}
        values={filters.tags}
        onChange={handleItemChange}
        onAllChange={handleAllChange}
      />
    </FilterBox>
  );
}
