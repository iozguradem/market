import { useState, useEffect } from "react";
import FilterBox from "../../FilterBox";
import SearchableListSelect from "../../Form/SearchableListSelect";
import { Option } from "../../../types/shared";
import { useAppSelector, useAppDispatch } from "../../../store/hooks";
import {
  pushBrand,
  removeBrand,
  pushMultipleBrand,
  removeMultipleBrand,
} from "../../../store/slices/filters";
import { getCompanies } from "../../../data";

export default function BrandsFilter() {
  const filters = useAppSelector((state) => state.filters);
  const [companies, setCompanies] = useState<Option[]>([]);
  const dispatch = useAppDispatch();

  const handleItemChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      dispatch(pushBrand(event.target.value));
    } else {
      dispatch(removeBrand(event.target.value));
    }
  };

  const handleAllChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    options: string[]
  ) => {
    if (event.target.checked) {
      dispatch(pushMultipleBrand(options));
    } else {
      dispatch(removeMultipleBrand(options));
    }
  };

  useEffect(() => {
    getCompanies().then((companies: Option[]) => setCompanies(companies));
  }, []);

  return (
    <FilterBox title="Brands">
      <SearchableListSelect
        placeholder="Search Brands"
        options={companies}
        values={filters.brands}
        onChange={handleItemChange}
        onAllChange={handleAllChange}
      />
    </FilterBox>
  );
}
