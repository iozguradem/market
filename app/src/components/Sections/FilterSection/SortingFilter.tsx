import FilterBox from "../../FilterBox";
import RadioButton from "../../Form/RadioButton";
import { Option } from "../../../types/shared";
import { useAppSelector, useAppDispatch } from "../../../store/hooks";
import { setSorting } from "../../../store/slices/filters";
import { SortingOptions } from "../../../types/shared";

const OPTIONS: Option[] = [
  {
    value: SortingOptions.PRICE_ASC,
    title: "Price low to high",
  },
  {
    value: SortingOptions.PRICE_DESC,
    title: "Price high to low",
  },
  {
    value: SortingOptions.DATE_ASC,
    title: "New to old",
  },
  {
    value: SortingOptions.DATE_DESC,
    title: "Old to new",
  },
];

export default function SortingFilter() {
  const filters = useAppSelector((state) => state.filters);
  const dispatch = useAppDispatch();

  const handleChange = (value: string) => {
    dispatch(setSorting(value as SortingOptions));
  };

  return (
    <FilterBox title="Sorting">
      <RadioButton
        name="sorting"
        items={OPTIONS}
        value={filters.sorting}
        onChange={handleChange}
      />
    </FilterBox>
  );
}
