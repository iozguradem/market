import { useAppSelector } from "../../store/hooks";
import { toCurrency } from "../../helpers";

export default function TotalBasketPrice() {
  const basket = useAppSelector((state) => state.basket);
  const totalPrice = basket.items.reduce((total, item) => {
    return total + item.count * item.product.price;
  }, 0);
  return <>{toCurrency(totalPrice)}</>;
}
