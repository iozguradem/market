# Market Application

This application is just a demonstration of a simple market application by using [React](https://reactjs.org/), [TypeScript](https://www.typescriptlang.org/) and [Redux](https://redux.js.org/).

## Folder Structure

There are two different application in the repository;

- api: The API that can be serve with [json-server](https://github.com/typicode/json-server)
- app: The frontend application that has been written by [React](https://reactjs.org/)
  - src
    - components: All reusable components
      - Button: All button components
      - FilterBox: Generic filter box that is used from two different places (Brand and Tag Filter)
      - Form: All form inputs
        - Checkbox: Used in `SearchableListSelect` components to be able to check items.
        - RadioButton: Used in sorting options:
        - SearchableListSelect: Developeed for brand and tag lists.
        - TextInpput: User in search inputs
        - ToggleButton: Used in product category selection
      - Header: The header component and its child components
      - PageContent: The general page content structure
      - Pagination: Pagination component that can be used different pages
      - Products: All product components
        - ProductCard: Card view of a product
        - ProductCategoryFilter: Filtering component the products by categories
        - ProductList: Product listing component
      - Sections: General sections of the page;
        - BasketSection: The section that is in the right of the page
        - FilterSection: The component that contains filters and sorting boxes
      - Shared: Some shared components that can be used from different components
    - data: API request functions
    - helpers: Some helpers
    - store: Redux structures
      - slices: reducer definitions
        - basket: The basket store. When the user adds or remove an item, it would be kept in this store.
        - filters: All filter selections would be kept in this store.
        - pagination: The user's current page selection and API paginate results are keeping in this store.
        - panels: Keeps the mobile panel visibility data.
    - types: API and shared types.

## Installation

In your machine, you can use the following command to pull the application;

```bash
$ git clone https://gitlab.com/iozguradem/market.git
$ cd market
```

To execute the API, you should use the following commands (in the `api` directory);

```bash
$ npm install -g json-server
$ json-server -p 4000 db.json
```

To execute the frontend application, you can use the following commands (in the `app` directory);

> You have to install [Yarn](https://yarnpkg.com/) in your local development environment.

```bash
$ yarn install
$ yarn start
```

## Features

- The frontend application has been written with React, TypeScript, [styled-components](https://styled-components.com/), and Redux.
- Pixel-perfect designed. Mobile design support.
- Filter, pagination, adding/remove to the basket product, and all other requested features.
- Reusable and tiny components.
